# docker-idee

Docker image for idee license server.

Or, you can build with source repository by yourself.

```sh
$ git clone https://gitlab.com/zwiebl.xyz/idea-license-server.git
$ cd idee
$ docker build -t zwiebl/idee .
```

## Usage

Just run a container like this:

```sh
$ sudo docker run --restart always -d --name idee-server \
-p 1027:1027 \
zwiebl/idee
```

or by docker-compose
```
docker-compose up -d app
```

```http
http://yourIp:1027
```
